behsigsoc <- function(results, datatype){
  
  if(datatype == "means"){
    # List to save plots to
    plot.list = list()
    
    # Facet labels
    max.labs = paste("Maximum Probability", sort(unique(results$Qdiff$max)))
    names(max.labs) = sort(unique(results$Qdiff$max))
    
    ratio.labs = paste("Probability Ratio", sort(unique(results$Qdiff$ratio)))
    names(ratio.labs) = sort(unique(results$Qdiff$ratio))
    
    facet.labeller = labeller(ratio=ratio.labs, max=max.labs)
    
    # Summarise sampling distribution of the mean for individual accuracy and rewards 
    plot.data = results$rewacc %>%
      # Summarise sampling distribution
      group_by(model, alphaS, ratio, max) %>%
      reframe(ind.acc.pop= mean(ind.acc.mean),
              ind.acc.lower = quantile(ind.acc.mean, probs = .025),
              ind.acc.upper = quantile(ind.acc.mean, probs = .975),
              reward.sum.pop = mean(reward.sum.mean),
              reward.sum.lower = quantile(reward.sum.mean, probs = .025),
              reward.sum.upper = quantile(reward.sum.mean, probs = .975))
    
    # Compare models with regards to individual rewards 
    p = plot.data %>%
      ggplot(aes(x=alphaS, fill=as.factor(model))) + 
      labs(x="Social Learning Rate", y="Individual Rewards",
           title = "Sampling Distribution of The Mean (Based on 10 Samples) \n") +
      scale_y_continuous(limits = c(0,1),  breaks=seq(0, 1, by=.2))+
      scale_x_continuous(breaks = seq(0, 1, by=.1))+
      scale_fill_viridis(discrete = T, begin = .1, end = .8, 
                         name="Model") +
      geom_hline(yintercept = .5, lty=2)+
      geom_pointrange(aes(y=reward.sum.pop, ymin=reward.sum.lower, ymax=reward.sum.upper), shape=21, size=1) +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    
    # Compare models with regards to individual accuracy 
    p = plot.data %>%
      ggplot(aes(x=alphaS, fill=as.factor(model))) + 
      labs(x="Social Learning Rate", y="Individual Accuracy",
           title = "Sampling Distribution of The Mean (Based on 10 Samples)  \n") +
      scale_y_continuous(limits = c(0,1),  breaks=seq(0, 1, by=.2))+
      scale_x_continuous(breaks = seq(0, 1, by=.1))+
      scale_fill_viridis(discrete = T, begin = .1, end = .8, 
                         name="Model") +
      geom_hline(yintercept = .5, lty=2)+
      geom_pointrange(aes(y=ind.acc.pop, ymin=ind.acc.lower, ymax=ind.acc.upper), shape=21, size=1) +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    # Compute pop mean for individual accuracy ~ time 
    plot.data = results$acc.frac %>%
      group_by(model, alphaS, ratio, max, time) %>%
      reframe(frac.corr.pop = mean(frac.corr))
    
    # Plot for model 2
    p = plot.data %>% filter(model == "m2.1") %>%
      ggplot(aes(x=time, y=frac.corr.pop, group=alphaS, col=alphaS)) +
      geom_line(alpha=.5, lty=2) +
      geom_line(aes(group=alphaS), data=plot.data %>% filter(model == "m2.1" & alphaS == 0)) +
      scale_y_continuous(limits = c(0.4,1),  breaks=seq(0, 1, by=.2))+
      scale_color_viridis(name="Social Learning Rate \n") +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      labs(x="Time", y="Individual Accuracy",
           title = "Estimated Population Mean (Based on 10 Samples). Decision Based Decision Biasing (m1.2): \n") +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    
    # Plot for model 3
    p = plot.data %>% filter(model == "m3.1") %>%
      ggplot(aes(x=time, y=frac.corr.pop, group=alphaS, col=alphaS)) +
      geom_line(alpha=.75, lty=2) +
      geom_line(aes(group=alphaS), data=plot.data %>% filter(model == "m3.1" & alphaS == 0)) +
      scale_y_continuous(limits = c(0.4,1),  breaks=seq(0, 1, by=.2))+
      scale_color_viridis(name="Social Learning Rate \n") +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      labs(x="Time", y="Individual Accuracy",
           title = "Estimated Population Mean (Based on 10 Samples): Decision Based Value Shaping (m1.3) \n") +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    # Summarize sampling distribution of the mean for switch rate
    plot.data = results$switches %>%
      group_by(model, alphaS, ratio, max) %>%
      reframe(switches.pop = mean(switches.mean), switches.lower=quantile(switches.mean, probs=.025), switches.upper=quantile(switches.mean, probs=.975))  
    
    # Plot
    p = plot.data %>%
      ggplot(aes(x=alphaS, fill=as.factor(model))) +
      labs(x="Social Learning Rate", y="Switch Rate",
           title = "Sampling Distribution of The Mean (Based on 10 Samples) \n") +
      scale_x_continuous(breaks = seq(0, 1, by=.1)) +
      scale_fill_viridis(discrete = T, begin = .1, end = .8, 
                         name="Model") +
      geom_pointrange(aes(y=switches.pop, ymin=switches.lower, ymax=switches.upper),
                      shape=21, size = 1) +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    # Compute pop mean for switch rate ~ time
    plot.data = results$switch.frac %>% 
      group_by(model, alphaS, time, ratio, max) %>%
      reframe(switch.frac.pop=mean(switch.frac))
    
    # Plot for model 2
    p = plot.data %>% filter(model == "m2.1") %>%
      ggplot(aes(x=time, y=switch.frac.pop, group=alphaS, col=alphaS)) +
      geom_line(alpha=.5, lty=2) +
      geom_line(data=plot.data %>% filter(model == "m2.1" & alphaS == 0)) +
      labs(x="Time", y="Switch Rate",
           title = "Estimated Population Mean (Based on 10 Samples). Decision Based Decision Biasing (m1.2): \n") +
      scale_y_continuous(limits = c(0,.6),  breaks=seq(0, 1, by=.2))+
      scale_colour_viridis(name="Social Learning Rate \n")+
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    # Plot switch rate for model 3
    p = plot.data %>% filter(model == "m3.1") %>%
      ggplot(aes(x=time, y=switch.frac.pop, group=alphaS, col=alphaS)) +
      geom_line(alpha=.5, lty=2) +
      geom_line(data=plot.data %>% filter(model == "m3.1" & alphaS == 0)) +
      labs(x="Time", y="Switch Rate",
           title = "Estimated Population Mean (Based on 10 Samples). Decision Based Value Shaping (m1.3): \n") +
      scale_colour_viridis(name="Social Learning Rate \n")+
      scale_y_continuous(limits = c(0,.6),  breaks=seq(0, 1, by=.2))+
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    
    # Compute pop mean Q-values ~ time for model 2
    plot.data = results$Qdiff  %>% 
      group_by(model, alphaS, time, ratio, max) %>%
      reframe(Qdiff.pop = mean(Qdiff.mean))
    
    # Plot for model 2
    p = plot.data %>% filter(model == "m2.1") %>%
      ggplot(aes(x=time, y=Qdiff.pop)) +
      geom_line(aes(y=Qdiff.pop, group=alphaS, col=alphaS), alpha=.5, lty=2) +
      geom_line(data=plot.data %>% filter(model == "m2.1" & alphaS == 0)) +
      scale_colour_viridis(name="Social Learning Rate \n")+
      labs(x="Time", y="Q[1] - Q[0]",
           title = "Estimated Population Mean (Based on 10 Samples). Decision Based Decision Biasing (m1.2): \n") +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
    
    # Plot model 3
    p = plot.data %>% filter(model == "m3.1") %>%
      ggplot(aes(x=time, y=Qdiff.pop)) +
      geom_line(aes(y=Qdiff.pop, group=alphaS, col=alphaS), alpha=.5, lty=2) +
      geom_line(data=plot.data %>% filter(model == "m2.1" & alphaS == 0)) +
      scale_colour_viridis(name="Social Learning Rate \n")+
      labs(x="Time", y="Q[1] - Q[0]",
           title = "Estimated Population Mean (Based on 10 Samples). Decision Based Value Shaping (m1.3): \n") +
      theme(text = element_text(size=20),
            plot.title = element_text(hjust = 0.5),
            plot.margin = margin(1,1,1,1, "cm")) +
      facet_grid(ratio ~ max, labeller = facet.labeller)
    p
    plot.list =  append(plot.list, list(p))
  }else if(datatype == "rawdata"){
    
    # Facet labels
    max.labs = paste("Maximum Probability", sort(unique(results$max)))
    names(max.labs) = sort(unique(results$max))
    
    ratio.labs = paste("Probability Ratio", sort(unique(results$ratio)))
    names(ratio.labs) = sort(unique(results$ratio))
    
    facet.labeller = labeller(ratio=ratio.labs, max=max.labs)
    
    ind.acc = list()
    ind.acc.time = list()
    switch.time = list()
    
    for(i in alphaDBD){
      
      
      p=results %>% filter(alphaS == i) %>%
        group_by(model, parcomb, alphaS, sim, session, trial, player, duration, ratio, max) %>%
        reframe(ind.acc = sum(decision)) %>% mutate(ind.acc = ind.acc / (duration+1)) %>%
        ggplot(aes(x=ind.acc, fill=as.factor(model))) +
        geom_density(alpha=.75) +
        scale_fill_viridis(discrete = T, begin = .1, end = .8, name="Model") +
        labs(x="Individual Accuracy", y="Density", title = paste("Simulations for a social learning weight of", i, "\n")) +
        theme_gray(base_size = 11) +
        theme(text = element_text(size=rel(5)),
              strip.text.x = element_text(size=rel(5)),
              strip.text.y = element_text(size=rel(5)), 
              legend.text = element_text(size=rel(5)), 
              plot.title = element_text(hjust = 0.5, size = rel(8)),
              plot.margin = margin(1,1,1,1, "cm"))+ 
        facet_grid(ratio ~  max)
      ind.acc = append(ind.acc, list(p))
      
      p = results %>% filter(alphaS == i) %>%
        group_by(model, parcomb, alphaS, sim, time, ratio, max) %>%
        reframe(ind.acc = mean(decision)) %>%
        ggplot(aes(x=time, y=ind.acc, group=interaction(model, sim), col=as.factor(model))) +
        geom_line(alpha=.75) +
        scale_color_viridis(discrete = T, begin = .1, end = .8, name="Model") +
        labs(x="Time", y="Individual Accuracy", title = paste("Simulations for a social learning weight of", i, "\n")) +
        theme_gray(base_size = 11) +
        theme(text = element_text(size=rel(5)),
              strip.text.x = element_text(size=rel(5)),
              strip.text.y = element_text(size=rel(5)), 
              legend.text = element_text(size=rel(5)), 
              plot.title = element_text(hjust = 0.5, size = rel(8)),
              plot.margin = margin(1,1,1,1, "cm"))+ 
        facet_grid(ratio ~ max)
      ind.acc.time = append(ind.acc.time, list(p))
      
      p = results %>% filter(alphaS == i) %>%
        group_by(model, alphaS, sim, session, trial, player, duration, ratio, max) %>%
        mutate(switch = ifelse(lag(decision) != decision, 1, 0)) %>%
        drop_na(switch) %>%
        group_by(model, parcomb, alphaS, sim, time, ratio, max) %>%
        reframe(switch.frac = mean(switch))  %>%
        ggplot(aes(x=time, y=switch.frac, group=interaction(model, sim), col=as.factor(model))) +
        geom_line(alpha=.75) +
        scale_color_viridis(discrete = T, begin = .1, end = .8, name="Model") +
        labs(x="Time", y="Switch Rate", title = paste("Simulations for a social learning weight of", i, "\n")) +
        theme_gray(base_size = 11) +
        theme(text = element_text(size=rel(5)),
              strip.text.x = element_text(size=rel(5)),
              strip.text.y = element_text(size=rel(5)), 
              legend.text = element_text(size=rel(5)), 
              plot.title = element_text(hjust = 0.5, size = rel(8)),
              plot.margin = margin(1,1,1,1, "cm"))+ 
        facet_grid(ratio ~ max, labeller = facet.labeller) 
      switch.time = append(switch.time, list(p))
      
      
    }
    plot.list = c(ind.acc, ind.acc.time, switch.time)
    
  }
  
  
  

  #### Return plot list ####
  return(plot.list)
}
