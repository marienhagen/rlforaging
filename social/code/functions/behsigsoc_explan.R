behsigsoc_explan <- function(results){
  
  # List to save plots to
  plot.list = list()
  
  max.labs = paste("Maximum Catch Probability", sort(unique(results$max)))
  names(max.labs) = sort(unique(results$max))
  
  ratio.labs = paste("Probability Ratio", sort(unique(results$ratio)))
  names(ratio.labs) = sort(unique(results$ratio))
  
  facet.labeller = labeller(ratio=ratio.labs, max=max.labs)
  
  # # Plot the Q values for  model 1
  # plot.data = results %>%
  #   pivot_longer(c(Q1, Q2), names_to = "option", values_to = "Q")
  # p = plot.data %>% filter(model == "m1.1") %>%
  #   ggplot(aes(x=Q, group=as.factor( option), fill=as.factor(option))) +
  #   geom_density(alpha=.75) +
  #   scale_fill_viridis(discrete = T, name="Option", labels=c("Lower Yielding", "Higher Yielding")) +
  #   labs(x="Q-Values", y="Density", title = "Distribution of Q-Values for Asocial Reinforcement Learning") +
  #   theme(text = element_text(size=20),
  #         plot.title = element_text(hjust = 0.5),
  #         plot.margin = margin(1,1,1,1, "cm")) +
  #   facet_grid(ratio ~ max, labeller = facet.labeller)
  # p
  # plot.list =  append(plot.list, list(p))
  
  # Plot the Q values for each model
  plot.data = results %>%
    pivot_longer(c(Q1, Q2), names_to = "option", values_to = "Q")
  p = plot.data %>%
    ggplot(aes(x=Q, y=as.factor(model),group=interaction(model, option), fill=as.factor(option))) +
    geom_density_ridges(alpha=.75) +
    scale_fill_viridis(discrete = T, name="Option", labels=c("Lower Yielding", "Higher Yielding")) +
    labs(x="Q-Values", y="Model") +
    theme(text = element_text(size=20),
          plot.title = element_text(hjust = 0.5),
          plot.margin = margin(1,1,1,1, "cm")) +
    facet_grid(ratio ~ max, labeller = facet.labeller)
  p
  plot.list =  append(plot.list, list(p))
  

  # Plot the prediction errors for the social models
  plot.data = results %>% filter(model != "m1.1") %>%
    pivot_longer(c(dpe1, dpe2, qpe1, qpe2), names_to = "option", values_to = "pe") %>%
    mutate(option = as.numeric(str_extract(option, "\\d+"))) %>%
    drop_na(pe)
  
  p = plot.data %>%
    ggplot(aes(x=pe, y=as.factor(model),group=interaction(model, option), fill=as.factor(option))) +
    geom_density_ridges(alpha=.75) +
    scale_fill_viridis(discrete = T, name="Option", labels=c("Lower Yielding", "Higher Yielding")) +
    labs(x="Social Learning Prediction Errors", y="Model") +    theme(text = element_text(size=20),
          plot.title = element_text(hjust = 0.5),
          plot.margin = margin(1,1,1,1, "cm")) +
    facet_grid(ratio ~ max, labeller = facet.labeller)
  p
  plot.list =  append(plot.list, list(p))
  

  # Plot the effect of the initial distribution of players
  plot.data = results %>%
    group_by(model, sim, session, trial) %>%
    mutate(initdis = sum(decision[which( time == 0)])) %>%
    group_by(model, ratio, max, time, initdis) %>%
    reframe(frac.corr =mean(decision)) 
  
  p = plot.data %>% filter(model == "m2.1") %>%
    ggplot(aes(x=time, y=frac.corr, group=as.factor(initdis), col=as.factor(initdis))) +
    labs(x="Time", y="Individual Accuracy", col="Number of correct individuals at t == 0",
         title = "Decision-Based Decision-Biasing \n") +
    geom_line() +
    scale_color_viridis(discrete = T, end = .8) +
    theme(text = element_text(size=20),
          plot.title = element_text(hjust = 0.5),
          plot.margin = margin(1,1,1,1, "cm")) +
    facet_grid(ratio ~ max, labeller = facet.labeller)
  p
  plot.list =  append(plot.list, list(p))
  
  
  p = plot.data %>% filter(model == "m3.1") %>%
    ggplot(aes(x=time, y=frac.corr,  group=as.factor(initdis), col=as.factor(initdis))) +
    labs(x="Time", y="Individual Accuracy", col="Number of correct individuals at t == 0",
         title = "Decision-Based Value Shaping \n") +
    geom_line() +
    scale_color_viridis(discrete = T, end = .8) +
    theme(text = element_text(size=20),
          plot.title = element_text(hjust = 0.5),
          plot.margin = margin(1,1,1,1, "cm")) +
    facet_grid(ratio ~ max, labeller = facet.labeller)
  p
  plot.list =  append(plot.list, list(p))
  

  
  #### Return plot list ####
  return(plot.list)
}
