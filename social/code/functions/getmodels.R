getmodels <- function(hierarch=FALSE){
  
  if(hierarch == FALSE){
    
  
    models = list(
      
      # Solo, DB, VS
      name = list(
        "m1.1",
        "m2.1",
        "m2.2",
        "m3.1",
        "m3.2"
      ),
      
      sim = list(
        "m1.1.sim",
        "m2.1.sim",
        "m2.2.sim",
        "m3.1.sim",
        "m3.2.sim"
      ),
      
      # Without gq for loglik
      stan = list(
        "../stan/m1.1.fixed.stan",
        "../stan/m2.1.fixed.stan",
        "../stan/m2.2.fixed.stan",
        "../stan/m3.1.fixed.stan",
        "../stan/m3.2.fixed.stan"
      ),
      # With gq for loglik
      stan.loglik = list(
        "../stan/m1.1.fixed.ll.stan",
        "../stan/m2.1.fixed.ll.stan",
        "../stan/m2.2.fixed.ll.stan",
        "../stan/m3.1.fixed.ll.stan",
        "../stan/m3.2.fixed.ll.stan"
      ),
      
      # Fixed parameters
      fixed.pars = list(
        # M1.1
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M2.1
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M2.2
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M3.1
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M3.2
        list(
          "Q.init" = .5,
          "C.init" = 0
        )
      ),
      
      # Free parameters
      free.pars = list(
        # M1.1
        list(
          "alphaQN[1]" = c(0, 1), # Individual learning rate for negative rpes [MAXIMUM]
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1), # Individual learning rate for positive rpes [MAXIMUM]
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),     # Inverse temp
          "betaC" = c(-4, 4)      # Autocorrelation
        ),
        # M2.1
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaDBD" = c(0, 1)    # Social learning rate DB
        ),
        # M2.2
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaDBD[1,1]" = c(0, 1),
          "alphaDBD[2,1]" = c(0, 1),
          "alphaDBD[3,1]" = c(0, 1),
          "alphaDBD[1,2]" = c(0, 1),
          "alphaDBD[2,2]" = c(0, 1),
          "alphaDBD[3,2]" = c(0, 1),
          "alphaDBD[1,3]" = c(0, 1),
          "alphaDBD[2,3]" = c(0, 1),
          "alphaDBD[3,3]" = c(0, 1),
          "alphaDBD[1,4]" = c(0, 1),
          "alphaDBD[2,4]" = c(0, 1),
          "alphaDBD[3,4]" = c(0, 1)
        ),
        # M3.1
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaVSD" = c(0, 1)     # Social learning rate VS
        ),
        # M3.2
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaVSD[1,1]" = c(0, 1),
          "alphaVSD[2,1]" = c(0, 1),
          "alphaVSD[3,1]" = c(0, 1),
          "alphaVSD[1,2]" = c(0, 1),
          "alphaVSD[2,2]" = c(0, 1),
          "alphaVSD[3,2]" = c(0, 1),
          "alphaVSD[1,3]" = c(0, 1),
          "alphaVSD[2,3]" = c(0, 1),
          "alphaVSD[3,3]" = c(0, 1),
          "alphaVSD[1,4]" = c(0, 1),
          "alphaVSD[2,4]" = c(0, 1),
          "alphaVSD[3,4]" = c(0, 1)
        )
        
      ),
      
      # Just which parameters are nested like learningrate[MAXIMUM]
      free.pars.struct = list(
        # M1.1
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC"
        ),
        # M2.1
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaDBD"
        ),
        # M2.2
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaDBD" = list(
            "alphaDBD[1,1]",
            "alphaDBD[2,1]",
            "alphaDBD[3,1]",
            "alphaDBD[1,2]",
            "alphaDBD[2,2]",
            "alphaDBD[3,2]",
            "alphaDBD[1,3]",
            "alphaDBD[2,3]",
            "alphaDBD[3,3]",
            "alphaDBD[1,4]",
            "alphaDBD[2,4]",
            "alphaDBD[3,4]"
          )
        ),
        # M3.1
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaVSD"
        ),
        # M3.2
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaVSD" = list(
            "alphaVSD[1,1]",
            "alphaVSD[2,1]",
            "alphaVSD[3,1]",
            "alphaVSD[1,2]",
            "alphaVSD[2,2]",
            "alphaVSD[3,2]",
            "alphaVSD[1,3]",
            "alphaVSD[2,3]",
            "alphaVSD[3,3]",
            "alphaVSD[1,4]",
            "alphaVSD[2,4]",
            "alphaVSD[3,4]"
          )
        )
      )
    )
    
    
  }else if(hierarch == TRUE){
    models = list(
      
      # Solo, DB, VS
      name = list(
        "m1.1.hierarch",
        "m2.1.hierarch",
        "m2.2.hierarch",
        "m3.1.hierarch",
        "m3.2.hierarch"
      ),
      
      sim = list(
        NA,
        NA,
        NA,
        NA,
        NA
      ),
      
      # Without gq for loglik
      stan = list(
        "../stan/m1.1.hierarch.stan",
        "../stan/m2.1.hierarch.stan",
        "../stan/m2.2.hierarch.stan",
        "../stan/m3.1.hierarch.stan",
        "../stan/m3.2.hierarch.stan"
      ),
      # With gq for loglik
      stan.loglik = list(
        "../stan/m1.1.hierarch.ll.stan",
        "../stan/m2.1.hierarch.ll.stan",
        "../stan/m2.2.hierarch.ll.stan",
        "../stan/m3.1.hierarch.ll.stan",
        "../stan/m3.2.hierarch.ll.stan"
      ),
      
      # Fixed parameters
      fixed.pars = list(
        # M1.1
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M2.1
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M2.2
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M3.1
        list(
          "Q.init" = .5,
          "C.init" = 0
        ),
        # M3.2
        list(
          "Q.init" = .5,
          "C.init" = 0
        )
      ),
      
      # Free parameters
      free.pars.pop = list(
        # M1.1
        list(
          "alphaQN[1]" = c(0, 1), # Individual learning rate for negative rpes [MAXIMUM]
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1), # Individual learning rate for positive rpes [MAXIMUM]
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),     # Inverse temp
          "betaC" = c(-4, 4)      # Autocorrelation
        ),
        # M2.1
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaDBD" = c(0, 1)    # Social learning rate DB
        ),
        # M2.2
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaDBD[1,1]" = c(0, 1),
          "alphaDBD[2,1]" = c(0, 1),
          "alphaDBD[3,1]" = c(0, 1),
          "alphaDBD[1,2]" = c(0, 1),
          "alphaDBD[2,2]" = c(0, 1),
          "alphaDBD[3,2]" = c(0, 1),
          "alphaDBD[1,3]" = c(0, 1),
          "alphaDBD[2,3]" = c(0, 1),
          "alphaDBD[3,3]" = c(0, 1),
          "alphaDBD[1,4]" = c(0, 1),
          "alphaDBD[2,4]" = c(0, 1),
          "alphaDBD[3,4]" = c(0, 1)
        ),
        # M3.1
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaVSD" = c(0, 1)     # Social learning rate VS
        ),
        # M3.2
        list(
          "alphaQN[1]" = c(0, 1),
          "alphaQN[2]" = c(0, 1),
          "alphaQN[3]" = c(0, 1),
          "alphaQP[1]" = c(0, 1),
          "alphaQP[2]" = c(0, 1),
          "alphaQP[3]" = c(0, 1),
          "betaQ" = c(0, 10),
          "betaC" = c(-4, 4),
          
          "alphaVSD[1,1]" = c(0, 1),
          "alphaVSD[2,1]" = c(0, 1),
          "alphaVSD[3,1]" = c(0, 1),
          "alphaVSD[1,2]" = c(0, 1),
          "alphaVSD[2,2]" = c(0, 1),
          "alphaVSD[3,2]" = c(0, 1),
          "alphaVSD[1,3]" = c(0, 1),
          "alphaVSD[2,3]" = c(0, 1),
          "alphaVSD[3,3]" = c(0, 1),
          "alphaVSD[1,4]" = c(0, 1),
          "alphaVSD[2,4]" = c(0, 1),
          "alphaVSD[3,4]" = c(0, 1)
        )
        
      ),
      
      # Just which parameters are nested like learningrate[MAXIMUM]
      free.pars.pop.struct = list(
        # M1.1
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC"
        ),
        # M2.1
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaDBD"
        ),
        # M2.2
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaDBD" = list(
            "alphaDBD[1,1]",
            "alphaDBD[2,1]",
            "alphaDBD[3,1]",
            "alphaDBD[1,2]",
            "alphaDBD[2,2]",
            "alphaDBD[3,2]",
            "alphaDBD[1,3]",
            "alphaDBD[2,3]",
            "alphaDBD[3,3]",
            "alphaDBD[1,4]",
            "alphaDBD[2,4]",
            "alphaDBD[3,4]"
          )
        ),
        # M3.1
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaVSD"
        ),
        # M3.2
        list(
          "alphaQN" = list(
            "alphaQN[1]",
            "alphaQN[2]",
            "alphaQN[3]"
          ),
          "alphaQP" = list(
            "alphaQP[1]",
            "alphaQP[2]",
            "alphaQP[3]"
          ),
          "betaQ",
          "betaC",
          "alphaVSD" = list(
            "alphaVSD[1,1]",
            "alphaVSD[2,1]",
            "alphaVSD[3,1]",
            "alphaVSD[1,2]",
            "alphaVSD[2,2]",
            "alphaVSD[3,2]",
            "alphaVSD[1,3]",
            "alphaVSD[2,3]",
            "alphaVSD[3,3]",
            "alphaVSD[1,4]",
            "alphaVSD[2,4]",
            "alphaVSD[3,4]"
          )
        )
      )
    )
  }
  
  
  return(models)
}
