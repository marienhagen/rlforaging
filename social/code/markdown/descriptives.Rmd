---
output: html_document
editor_options: 
  chunk_output_type: console
---
# Descriptives
```{r setup-descriptives, include=FALSE}
knitr::opts_chunk$set(echo=FALSE, results = 'markup', warning = TRUE)

library(knitr)
library(dplyr)
library(tidyr)
library(purrr)
library(tibble)
library(stringr)
library(reshape2)

library(ggplot2)
library(ggpubr)
library(viridis)
library(ggdist)

library(rstan)
library(tidybayes)
library(bayesplot)
library(randtoolbox)

# Source functions
setwd("~/Users/MarienhagenJonathan/thesisvc/social/code/functions")
source("descriptives.plot.R")


# Change wd
setwd("~/Users/MarienhagenJonathan/thesisvc/social/code/markdown")

```
```{r, directories-descriptives, include=FALSE}
if(!dir.exists("../../results/descriptives")){dir.create("../../results/descriptives")}
```

```{r, data-descriptives, echo=TRUE}
# Read data
path = "~/Users/MarienhagenJonathan/thesis/data/data_discrete_1s.csv"
d = read.csv(path,colClasses = c(rep(NA, 8), rep("character", 2), rep(NA, 4)))

# Set player id unique across sessions
d = d %>% mutate(id = (session - 1) * 5  + player)

# Get solo trials
d = d %>% filter(social.fac %in% c(1, 2))

# Compute observed decisions (as was done for synthetic data)
d=d %>%
  mutate(decision = correct) %>% mutate(reward = catch) %>% mutate(nplayers = 5)
  # arrange(session, trial, player, time.rounded) %>%
  # group_by(session, trial, player) %>%
  # mutate(decision.lag = lag(decision)) %>%
  # group_by(session, trial, time.rounded) %>%
  # mutate(obs.dec.2 = sum(decision.lag)) %>% # Number of individuals choosing higher yielding patch
  # ungroup() %>%
  # mutate(obs.dec.2 = obs.dec.2 - decision.lag) %>% # Subtract individual's own choice
  # ungroup() %>%
  # mutate(obs.dec.1 = nplayers - 1 - obs.dec.2) %>% # Lower yielding patch
  # select(-decision.lag) %>%
  # mutate(decision = decision + 1) %>% # Transform [0, 1] to [1, 2]
  # relocate(obs.dec.1, obs.dec.2,.after = nplayers)


```

```{r, plot-descriptives}
plot.list = descriptives.plot(experimental.data = d, social = T)
ggexport(plotlist = plot.list, width = 2560, height = 1440,
           filename = paste("~/Users/MarienhagenJonathan/thesisvc/social/results/descriptives", "descriptives.jpeg", sep = "/"))
```

```{r, calc-descriptives}
# Sample sd func
S = function(x){
  sqrt(sum((x - mean(x))^2) / length(x))
} 

# Rewards
experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(reward.sum = sum(reward) / (duration+1)) %>%
    group_by(social.fac) %>%
    reframe(mu = mean(reward.sum), s = S(reward.sum), skew=skewness(reward.sum)) %>%
    mutate(across(everything()), round(., digits = 2))

experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(reward.sum = sum(reward) / (duration+1)) %>%
    group_by(ratio, social.fac) %>%
    reframe(mu = mean(reward.sum), s = S(reward.sum), skew=skewness(reward.sum))%>%
    ungroup() %>%
    mutate(across(where(is.numeric), \(x) round(x, digits = 2)))


experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(reward.sum = sum(reward) / (duration+1)) %>%
    group_by(max, social.fac) %>%
    reframe(mu = mean(reward.sum), s = S(reward.sum), skew=skewness(reward.sum))

experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(reward.sum = sum(reward) / (duration+1)) %>%
    group_by(ratio, max, social.fac) %>%
    reframe(mu = mean(reward.sum), s = S(reward.sum), skew=skewness(reward.sum))






#Accuracy
experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(ind.acc = mean(decision)) %>%
    group_by(social.fac) %>%
    reframe(mdn = median(ind.acc), lower = quantile(ind.acc, probs = .25), upper=quantile(ind.acc, probs = .75)) %>%
    ungroup() %>%
    mutate(across(where(is.numeric), \(x) round(x, digits = 2)))

experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(ind.acc = mean(decision)) %>%
    group_by(ratio, social.fac) %>%
    reframe(mdn = median(ind.acc), s = S(ind.acc), skew=skewness(ind.acc)) %>%
    ungroup() %>%
    mutate(across(where(is.numeric), \(x) round(x, digits = 3)))

experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(ind.acc = mean(decision)) %>%
    group_by(max, social.fac) %>%
    reframe(mdn = median(ind.acc), s = S(ind.acc), skew=skewness(ind.acc)) %>%
    ungroup() %>%
    mutate(across(where(is.numeric), \(x) round(x, digits = 3)))

experimental.data.sum = experimental.data %>% 
    group_by(session, trial, id, ratio, max, duration, social.fac) %>%
    reframe(ind.acc = mean(decision)) %>%
    group_by(ratio, max, social.fac) %>%
    reframe(mdn = median(ind.acc), lower = quantile(ind.acc, probs = .25), upper=quantile(ind.acc, probs = .75)) %>%
    ungroup() %>%
    mutate(across(where(is.numeric), \(x) round(x, digits = 3)))



```
